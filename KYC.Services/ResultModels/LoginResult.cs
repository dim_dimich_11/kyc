﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KYC.Services.ResultModels
{
    public class LoginResult
    {
        public string Token { get; set; }
    }
}
