﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KYC.Services.ResultModels
{
    public class GetParticipantStatusResult
    {
        [JsonProperty("current_status")]
        public string CurrentStatus { get; set; }

        [JsonProperty("ico_owner_status")]
        public string IcoOwnerStatus { get; set; }
    }
}
