﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using KYC.Services.Models;

namespace KYC.Services.Interfaces
{
    public interface IRequestSenderService
    {
        Task<RequestResult<TEntity>> PostRequestAsync<TEntity>(string url, string jsonContent, List<KeyValuePair<string, string>> parameters = null,
            List<KeyValuePair<string, string>> headers = null);

        Task<RequestResult<TEntity>> GetRequestAsync<TEntity>(string url, List<KeyValuePair<string, string>> parameters,
            List<KeyValuePair<string, string>> headers = null);

        Task<RequestResult<TEntity>> PutRequestAsync<TEntity>(string url, string jsonContent, List<KeyValuePair<string, string>> parameters,
            List<KeyValuePair<string, string>> headers = null);

        Task<RequestResult<TEntity>> PatchRequestAsync<TEntity>(string url, string jsonContent, List<KeyValuePair<string, string>> parameters,
            List<KeyValuePair<string, string>> headers = null);
    }
}
