﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using KYC.Services.Models;

namespace KYC.Services.Interfaces
{
    public interface IUserService
    {
        Task<RequestResult<TEntity>> AddIndividualParticipantDataAsync<TEntity>(string url, string jsonContent,
            List<KeyValuePair<string, string>> headers, List<KeyValuePair<string, string>> parameters);
    }
}
