﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KYC.Services.Enums;
using KYC.Services.Extensions;
using KYC.Services.Interfaces;
using KYC.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KYC.Services
{
    public class RequestSenderService : IRequestSenderService
    {
        private readonly HttpClient _client;

        public RequestSenderService()
        {
            _client = new HttpClient();
        }

        public async Task<RequestResult<TEntity>> GetRequestAsync<TEntity>(string url, List<KeyValuePair<string, string>> parameters, List<KeyValuePair<string, string>> headers = null)
        {
            DefaultInitialize(ref url, _client, parameters, headers);

            var sendResult = await _client.GetAsync(url);
            var requestResult = await ParseRequestResult<TEntity>(sendResult);

            return requestResult;
        }

        public async Task<RequestResult<TEntity>> PatchRequestAsync<TEntity>(string url, string jsonContent,
            List<KeyValuePair<string, string>> parameters, List<KeyValuePair<string, string>> headers = null)
        {
            DefaultInitialize(ref url, _client, parameters, headers);

            var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            var sendResult = await _client.PatchAsync(url, content);
            var requestResult = await ParseRequestResult<TEntity>(sendResult);

            return requestResult;
        }

        public async Task<RequestResult<TEntity>> PostRequestAsync<TEntity>(string url, string jsonContent,
            List<KeyValuePair<string, string>> parameters = null, List<KeyValuePair<string, string>> headers = null)
        {
            DefaultInitialize(ref url, _client, parameters, headers);

            var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            var sendResult = await _client.PostAsync(url, content);

            var requestResult = await ParseRequestResult<TEntity>(sendResult);

            return requestResult;
        }

        public async Task<RequestResult<TEntity>> PutRequestAsync<TEntity>(string url, string jsonContent,
            List<KeyValuePair<string, string>> parameters, List<KeyValuePair<string, string>> headers = null)
        {
            DefaultInitialize(ref url, _client, parameters, headers);

            var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            var sendResult = await _client.PutAsync(url, content);

            var requestResult = await ParseRequestResult<TEntity>(sendResult);

            return requestResult;
        }

        private async Task<RequestResult<TEntity>> ParseRequestResult<TEntity>(HttpResponseMessage response)
        {
            var requestResult = new RequestResult<TEntity>();

            var resultContent = await response.Content.ReadAsStringAsync();

            if (string.IsNullOrEmpty(resultContent))
            {
                requestResult.Error.StatusCode = response.StatusCode;
                requestResult.Error.Description = response.ReasonPhrase;
            }
            else
            {
                var jObject = JObject.Parse(resultContent);

                if (!response.IsSuccessStatusCode)
                {
                    requestResult.Error.StatusCode = response.StatusCode;
                    requestResult.Error.Description = response.StatusCode == HttpStatusCode.BadRequest
                        ? jObject["error"].Value<string>()
                        : jObject["message"].Value<string>();
                }
                else
                {
                    requestResult.Success = true;
                    requestResult.Result = JsonConvert.DeserializeObject<TEntity>(resultContent);
                }
            }

            return requestResult;
        }

        private static void DefaultInitialize(ref string url, HttpClient client,
            List<KeyValuePair<string, string>> parameters, List<KeyValuePair<string, string>> headers)
        {
            url = url.AddParamsToUrl(parameters);
            client.AddHeaders(headers);
        }
    }
}
