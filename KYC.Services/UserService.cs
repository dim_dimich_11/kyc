﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KYC.DAL.Enums;
using KYC.DAL.Interfaces;
using KYC.DAL.Models;
using KYC.Services.Interfaces;
using KYC.Services.Models;

namespace KYC.Services
{
    public class UserService : IUserService
    {
        private readonly IRequestSenderService _requestSenderService;
        private readonly ITestingUserRepository _testingUserRepository;

        public UserService(IRequestSenderService requestSenderService, ITestingUserRepository testingUserRepository)
        {
            _requestSenderService = requestSenderService;
            _testingUserRepository = testingUserRepository;
        }

        public async Task<RequestResult<TEntity>> AddIndividualParticipantDataAsync<TEntity>(string url, string jsonContent, List<KeyValuePair<string, string>> headers, List<KeyValuePair<string, string>> parameters)
        {
            var requestResult =
                await _requestSenderService.PutRequestAsync<TEntity>(url, jsonContent, parameters, headers);

            if (requestResult.Success)
            {
                var user = new TestingUser
                {
                    KycOwner = parameters.FirstOrDefault(x => x.Key == "kycOwner").Value,
                    UuId = parameters.FirstOrDefault(x => x.Key == "uuid").Value,
                    Status = UserStatus.New
                };

                await _testingUserRepository.CreateTestingUserAsync(user);
            }

            return requestResult;
        }
    }
}
