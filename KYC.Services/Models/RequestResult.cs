﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KYC.Services.Models
{
    public class RequestResult<T>
    {
        [JsonIgnore]
        public bool Success { get; set; }
        public Error Error { get; set; } = new Error();

        public T Result { get; set; }
    }
}
