﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KYC.Services.Dictionaries
{
    public static class KycUrls
    {
        public static string Login => Urls["Login"];
        public static string GetParticipantStatus => Urls["GetPacticipantStatus"];
        public static string AddEmptyParticipant => Urls["AddEmptyParticipant"];
        public static string SetParticipantOwnerStatus => Urls["SetParticipantOwnerStatus"];
        public static string InsertFileToKyc => Urls["InsertFileToKyc"];
        public static string SendIndividualFormDetail => Urls["SendIndividualFormDetail"];
        public static string GetProfile => Urls["GetProfile"];

        private static readonly Dictionary<string, string> Urls = new Dictionary<string, string>
        {
            {"Login","https://api.coinfirm.io/v2/auth/login" },
            {"AddEmptyParticipant","https://api.coinfirm.io/v2/kyc/customers/" },
            {"GetPacticipantStatus","https://api.coinfirm.io/v2/kyc/status/" },
            {"SetParticipantOwnerStatus","https://api.coinfirm.io/v2/kyc/status/" },
            {"InsertFileToKyc","https://api.coinfirm.io/v2/kyc/files/" },
            {"SendIndividualFormDetail","https://api.coinfirm.io/v2/kyc/forms/" },
            {"GetProfile","https://api.coinfirm.io/v2/users/me" }
        };
    }
}
