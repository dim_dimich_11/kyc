﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KYC.Services.Enums
{
    public enum Request
    {
        Get,
        Post,
        Put,
        Patch,
        Update,
        Delete
    }
}
