﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.Dashboard;
using Hangfire.SqlServer;
using Hangfire.Storage;
using KYC.DAL.Interfaces;
using KYC.DAL;
using KYC.DAL.Infrastructure;
using KYC.DAL.Models;
using KYC.Filters;
using KYC.Services;
using KYC.Services.Dictionaries;
using KYC.Services.Interfaces;
using KYC.Services.ResultModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace KYC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddHangfire(x => x.UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection")));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //services
            services.AddScoped<IRequestSenderService, RequestSenderService>();
            services.AddScoped<IUserService, UserService>();

            //repositories
            services.AddScoped<ITestingUserRepository, TestingUserRepository>();

            ////context
            services.AddScoped<DbContext, ApplicationContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IRequestSenderService requestSenderService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            JobStorage.Current = new SqlServerStorage(Configuration.GetConnectionString("DefaultConnection"));

            app.UseHangfireServer();
            app.UseHangfireDashboard("/dashboard");
            //app.UseHangfireDashboard("/dashboard", new DashboardOptions
            //{
            //    Authorization = new IDashboardAuthorizationFilter[] { new CustomAuthorizeFilter() }
            //});
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
