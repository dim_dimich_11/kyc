﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;

namespace KYC.HangfireConfiguration
{
    public class ContainerJobActivator : JobActivator
    {
        private IContainer _container;

        public ContainerJobActivator(IContainer container)
        {
            _container = container;
        }

        //public override object ActivateJob(Type type)
        //{
        //    return _container.Resolve(type);
        //}
    }
}
