﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KYC.Models
{
    public class Sow
    {
        [JsonProperty("sow_business_activities")]
        public bool SowBusinessActivities { get; set; }

        [JsonProperty("sow_stock_sales")]
        public bool SowStockSales { get; set; }

        [JsonProperty("sow_real_estate_sale")]
        public bool SowRealEstateSale { get; set; }

        [JsonProperty("sow_donation")]
        public bool SowDonation { get; set; }

        [JsonProperty("sow_inherited")]
        public bool SowInherited { get; set; }

        [JsonProperty("sow_crypto_trading")]
        public bool SowCryptoTrading { get; set; }

        [JsonProperty("sow_ico_contribution")]
        public bool SowIcoContribution { get; set; }

        [JsonProperty("sow_other")]
        public bool SowOther { get; set; }
    }
}
