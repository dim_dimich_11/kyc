﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KYC.Models
{
    public class IcoParticipantStatusParams
    {
        [Required]
        [JsonProperty("ico_owner_status")]
        public string IcoOwnerStatus { get; set; }
    }
}
