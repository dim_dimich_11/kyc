﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KYC.Models
{
    public class Address
    {
        public string Country { get; set; }

        public string City { get; set; }

        [JsonProperty("postcode")]
        public string PostalCode { get; set; }

        public string Street { get; set; }
    }
}
