﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KYC.Models
{
    public class Custom
    {
        [JsonProperty("mother_name")]
        public string MotherName { get; set; }
    }
}
