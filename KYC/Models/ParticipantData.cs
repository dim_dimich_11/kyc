﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KYC.ParameterModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KYC.Models
{
    public class ParticipantData
    {
        [JsonProperty("id_number")]
        public string IdNumber { get; set; }

        [JsonProperty("user_ip")]
        public string UserIp { get; set; }

        public string Type { get; set; }

        public string Email { get; set; }

        public Address Address { get; set; }
    }
}
