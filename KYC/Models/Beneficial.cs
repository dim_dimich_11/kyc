﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KYC.Models
{
    public class Beneficial
    {
        [JsonProperty("beneficial_name")]
        public string BeneficialName { get; set; }

        [JsonProperty("sow_real_estate_sale")]
        public bool SowRealEstateSale { get; set; }

        [JsonProperty("beneficial_pep")]
        public string BeneficialPep { get; set; }

        [JsonProperty("beneficial_nation")]
        public string BeneficialNation { get; set; }

        [JsonProperty("beneficial_adr")]
        public string BeneficialAdr { get; set; }

        [JsonProperty("beneficial_bdate")]
        public BDate BeneficialBDate { get; set; }

        [JsonProperty("beneficial_proc")]
        public int BeneficialProc { get; set; }

        [JsonProperty("beneficial_wealth_text")]
        public string BeneficialWealthText { get; set; }
    }
}
