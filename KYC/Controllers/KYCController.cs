﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KYC.Models;
using KYC.ParameterModels;
using KYC.Services.Dictionaries;
using KYC.Services.Interfaces;
using KYC.Services.Models;
using KYC.Services.ResultModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace KYC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KycController : ControllerBase
    {
        private readonly List<KeyValuePair<string, string>> _headers;
        private readonly IRequestSenderService _requestSenderService;
        private readonly IUserService _userService;

        public KycController(IRequestSenderService requestSenderService, IUserService userService)
        {
            _requestSenderService = requestSenderService;
            _userService = userService;

            _headers = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Authorization",
                    "Bearer gKKY6jftAUVOnsWaSREfcyYLMG6QTvvruwGLiSeuFVg9zDGJfWHXrK5B1jiNdgre"),
                new KeyValuePair<string, string>("Content-Type","application/json"),
                new KeyValuePair<string, string>("Accept","application/json")
            };
        }

        [HttpGet("status/current/{kycOwner}/{uuid}")]
        public async Task<IActionResult> GetCurrentParticipantStatusAsync([FromRoute(Name = "kycOwner")]string kycOwner, [FromRoute(Name = "uuid")]string uuid)
        {
            var parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("kycOwner",kycOwner),
                new KeyValuePair<string, string>("uuid",uuid)
            };

            var requestResult =
                await _requestSenderService.GetRequestAsync<GetParticipantStatusResult>(KycUrls.GetParticipantStatus,
                    parameters, _headers);

            return ReturnResult(requestResult);
        }

        [HttpPatch("status/{kycOwner}/{uuid}")]
        public async Task<IActionResult> SetParticipantOwnerStatusAsync([FromRoute(Name = "kycOwner")] string kycOwner,
            [FromRoute(Name = "uuid")] string uuid, IcoParticipantStatusParams icoParticipant)
        {
            var parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("kycOwner",kycOwner),
                new KeyValuePair<string, string>("uuid",uuid)
            };

            var jsonContent = JsonConvert.SerializeObject(icoParticipant);

            var requestResult = await _requestSenderService.PatchRequestAsync<object>(KycUrls.SetParticipantOwnerStatus,
                jsonContent, parameters, _headers);

            return ReturnResult(requestResult);
        }

        [HttpPut("customers/{kycOwner}")]
        public async Task<IActionResult> AddEmptyParticipantAsync([FromRoute(Name = "kycOwner")] string kycOwner,
            EmptyParticipantParams participant)
        {
            var parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("kycOwner",kycOwner)
            };

            var jsonContent = JsonConvert.SerializeObject(participant);

            var requestResult = await _requestSenderService.PutRequestAsync<object>(KycUrls.AddEmptyParticipant,
                jsonContent, parameters, _headers);

            return ReturnResult(requestResult);
        }

        [HttpPut("forms/individual/{kycOwner}/{uuid}")]
        public async Task<IActionResult> InsertIndividualParticipantDataAsync([FromRoute(Name = "kycOwner")] string kycOwner, [FromRoute(Name = "uuid")] string uuid, IndividualParticipantData individualParticipantParams)
        {
            var parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("kycOwner",kycOwner),
                new KeyValuePair<string, string>("uuid",uuid)
            };

            var jsonContent = JsonConvert.SerializeObject(individualParticipantParams);

            //var requestResult = await _requestSenderService.PutRequestAsync<object>(KycUrls.SendIndividualFormDetail,
            //    jsonContent, parameters, _headers);

            var requestResult =
                await _userService.AddIndividualParticipantDataAsync<object>(KycUrls.SendIndividualFormDetail,
                    jsonContent, _headers, parameters);

            return ReturnResult(requestResult);
        }

        //[HttpPut("forms/company/{kycOwner}/{uuid}")]
        //public async Task<IActionResult> InsertCompanyParticipantDataAsync([FromRoute(Name = "kycOwner")] string kycOwner,
        //                                                                            [FromRoute(Name = "uuid")] string uuid, CompanyParticipantData companyParticipantParams)
        //{
        //    await Task.Delay(100);
        //    return Ok();
        //}

        [HttpPost("files/{kycOwner}/{uuid}")]
        public async Task<IActionResult> InsertFileToKycProcessAsync([FromRoute(Name = "kycOwner")] string kycOwner,
                                                                                    [FromRoute(Name = "uuid")] string uuid, FileParams file)
        {
            var parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("kycOwner",kycOwner),
                new KeyValuePair<string, string>("uuid",uuid)
            };

            var jsonContent = JsonConvert.SerializeObject(file);

            var requestResult =
                await _requestSenderService.PostRequestAsync<object>(KycUrls.InsertFileToKyc, jsonContent, parameters,
                    _headers);

            return ReturnResult(requestResult);
        }

        private IActionResult ReturnResult<TEntity>(RequestResult<TEntity> requestResult)
        {
            if (!requestResult.Success)
            {
                return BadRequest(requestResult.Error);
            }

            return Ok(requestResult.Result);
        }
    }
}
