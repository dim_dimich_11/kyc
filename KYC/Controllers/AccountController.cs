﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using KYC.Models;
using KYC.Services.Dictionaries;
using KYC.Services.Interfaces;
using KYC.Services.ResultModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KYC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly string _token = "Bearer gKKY6jftAUVOnsWaSREfcyYLMG6QTvvruwGLiSeuFVg9zDGJfWHXrK5B1jiNdgre";//temporary
        private readonly List<KeyValuePair<string, string>> _headers;
        private readonly IRequestSenderService _requestSenderService;

        public AccountController(IRequestSenderService requestSenderService)
        {
            _requestSenderService = requestSenderService;
            _headers = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Authorization",_token)
            };
        }

        [AllowAnonymous]
        [HttpPost("auth/login")]
        public async Task<IActionResult> Login(LoginParrams model)
        {
            var jsonContent = JsonConvert.SerializeObject(model);

            var result = await _requestSenderService.PostRequestAsync<LoginResult>(KycUrls.Login, jsonContent);

            if (!result.Success)
            {
                return BadRequest(result.Error);
            }

            return Ok(result.Result);
        }

        [HttpGet("users/me")]
        public async Task<IActionResult> GetProfileAsync()
        {
            var result = await _requestSenderService.GetRequestAsync<object>(KycUrls.GetProfile, null, _headers);

            if (!result.Success)
            {
                return BadRequest(result.Error);
            }

            return Ok(result.Result);
        }
    }
}
