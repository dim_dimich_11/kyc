﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KYC.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KYC.ParameterModels
{
    public class IndividualParticipantData
    {
        [JsonProperty("id_number")]
        public string IdNumber { get; set; }

        [JsonProperty("user_ip")]
        public string UserIp { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("postcode")]
        public string PostalCode { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("pep")]
        public bool Pep { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("middle_name")]
        public string MiddleName { get; set; }

        [JsonProperty("nationality")]
        public string Nationality { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("phone_verified")]
        public string PhoneVerified { get; set; }

        [JsonProperty("bdate")]
        public BDate BDate { get; set; }

        [JsonProperty("fileFundsText")]
        public string FileFundsText { get; set; }

        [JsonProperty("sow")]
        public Sow Sow { get; set; }

        [JsonProperty("custom")]
        public List<Custom> Custom { get; set; }
    }
}
