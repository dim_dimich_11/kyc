﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KYC.Models;
using Newtonsoft.Json;

namespace KYC.ParameterModels
{
    public class CompanyParticipantData
    {
        [JsonProperty("id_number")]
        public string IdNumber { get; set; }

        [JsonProperty("user_ip")]
        public string UserIp { get; set; }

        public string Type { get; set; }

        public string Email { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        [JsonProperty("postcode")]
        public string PostalCode { get; set; }

        public string Street { get; set; }

        public List<Beneficial> Beneficials { get; set; }

        public List<Custom> Customs { get; set; }
    }
}
