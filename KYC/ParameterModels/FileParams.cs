﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace KYC.ParameterModels
{
    public class FileParams
    {
        [Required]
        [JsonProperty("type")]
        public string Type { get; set; }

        [Required]
        [JsonProperty("extension")]
        public string Extension { get; set; }

        [Required]
        [JsonProperty("data_base64")]
        public string Base64 { get; set; }
    }
}
