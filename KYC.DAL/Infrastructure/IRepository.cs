﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KYC.DAL.Enums;

namespace KYC.DAL.Infrastructure
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> predicate);
        Task<bool> UpdateOneAsync(TEntity entity);
        Task<List<TEntity>> FindManyAsync(Expression<Func<TEntity, bool>> predicate);
        Task InsertOneAsync(TEntity entity);
    }
}
