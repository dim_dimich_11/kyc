﻿using System;
using System.Collections.Generic;
using System.Text;
using KYC.DAL.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace KYC.DAL.Infrastructure
{
    public class ApplicationContext : IdentityDbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        public DbSet<TestingUser> TestingUsers { get; set; }
    }
}
