﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using KYC.DAL.Enums;
using KYC.DAL.Infrastructure;
using KYC.DAL.Interfaces;
using KYC.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace KYC.DAL
{
    public class TestingUserRepository : GenericRepository<TestingUser>, ITestingUserRepository
    {
        public TestingUserRepository(DbContext context) : base(context)
        {
        }

        public async Task CreateTestingUserAsync(TestingUser testingUser)
        {
            await InsertOneAsync(testingUser);
        }
    }
}
