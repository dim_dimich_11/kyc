﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using KYC.DAL.Enums;
using KYC.DAL.Infrastructure;
using KYC.DAL.Models;

namespace KYC.DAL.Interfaces
{
    public interface ITestingUserRepository : IRepository<TestingUser>
    {
        Task CreateTestingUserAsync(TestingUser testingUser);
    }
}
