﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KYC.DAL.Enums
{
    public enum UserStatus
    {
        Empty,
        New,
        InProgress,
        Incomplete,
        Low,
        Medium,
        High,
        Fail
    }
}
