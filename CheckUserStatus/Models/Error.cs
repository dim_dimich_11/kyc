﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace CheckUserStatus.Models
{
    public class Error
    {
        public HttpStatusCode StatusCode { get; set; }

        public string Description { get; set; }
    }
}
