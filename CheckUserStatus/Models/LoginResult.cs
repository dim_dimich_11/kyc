﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CheckUserStatus.Models
{
    public class LoginResult
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
