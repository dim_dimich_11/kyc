﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace CheckUserStatus.Models
{
    public class GetParticipantStatusResult
    {
        [JsonProperty("current_status")]
        public string CurrentStatus { get; set; }

        [JsonProperty("ico_owner_status")]
        public string IcoOwnerStatus { get; set; }
    }
}
