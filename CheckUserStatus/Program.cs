﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using CheckUserStatus.DbModels;
using CheckUserStatus.Enums;
using CheckUserStatus.Extensions;
using CheckUserStatus.Infrastructure;
using CheckUserStatus.Infrastructure.Interfaces;
using CheckUserStatus.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CheckUserStatus
{
    class Program
    {
        private static List<KeyValuePair<string, string>> _headers = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("Content-Type","application/json"),
            new KeyValuePair<string, string>("Accept","application/json")
        };

        private static readonly string GetParticipantStatusUrl = "https://api.coinfirm.io/v2/kyc/status/";
        private static readonly string LoginUrl = "https://api.coinfirm.io/v2/auth/login";

        private static ApplicationContext _context;
        private static GenericRepository<TestingUser> _userRepository;
        private static IConfiguration Configuration { get; set; }

        static async Task Main(string[] args = null)
        {
            Initialize();
            bool authorize = await AuthorizeInKyc();

            if (authorize)
            {
                var users = await _userRepository.FindManyAsync();
                await CheckUsersStatus(users);
            }
        }

        private static void Initialize()
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appconfig.json")
                .Build();

            _context = new ApplicationContext(Configuration.GetConnectionString("DefaultConnection"));
            _userRepository = new GenericRepository<TestingUser>(_context);
        }

        private static async Task<bool> AuthorizeInKyc()
        {
            bool authorized = false;
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                string email = Configuration["KYCEmail"];
                string password = Configuration["KYCPassword"];
                var content = "{\"email\": \"" + email + "\",\"password\": \"" + password + "\"}";
                var requestContent = new StringContent(content, Encoding.UTF8, "application/json");

                var requestResult = await httpClient.PostAsync(LoginUrl, requestContent);

                var parsedRequestResult = await ParseRequestResult<LoginResult>(requestResult);

                if (parsedRequestResult.Success)
                {
                    _headers.Add(new KeyValuePair<string, string>("Authorization",
                        $"Bearer {parsedRequestResult.Result.Token}"));
                    authorized = true;
                }
            }

            return authorized;
        }

        private static async Task CheckUsersStatus(List<TestingUser> users)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.AddHeaders(_headers);

                foreach (var user in users)
                {
                    List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("kycOwner",user.KycOwner),
                        new KeyValuePair<string, string>("uuid",user.UuId)
                    };

                    string url = GetParticipantStatusUrl.AddParamsToUrl(parameters);

                    var requestResult = await httpClient.GetAsync(url);

                    var parsedRequestResult = await ParseRequestResult<GetParticipantStatusResult>(requestResult);

                    if (parsedRequestResult.Success)
                    {
                        var status = (UserStatus)Enum.Parse(typeof(UserStatus), parsedRequestResult.Result.CurrentStatus,
                            true);

                        if (user.Status != status)
                        {
                            user.Status = UserStatus.High;
                            await _userRepository.UpdateOneAsync(user);
                        }
                    }
                }
            }
        }

        private static async Task<RequestResult<TEntity>> ParseRequestResult<TEntity>(HttpResponseMessage response)
        {
            var requestResult = new RequestResult<TEntity>();

            var resultContent = await response.Content.ReadAsStringAsync();

            if (string.IsNullOrEmpty(resultContent))
            {
                requestResult.Error.StatusCode = response.StatusCode;
                requestResult.Error.Description = response.ReasonPhrase;
            }
            else
            {
                var jObject = JObject.Parse(resultContent);

                if (!response.IsSuccessStatusCode)
                {
                    requestResult.Error.StatusCode = response.StatusCode;
                    requestResult.Error.Description = response.StatusCode == HttpStatusCode.BadRequest
                        ? jObject["error"].Value<string>()
                        : jObject["message"].Value<string>();
                }
                else
                {
                    requestResult.Success = true;
                    requestResult.Result = JsonConvert.DeserializeObject<TEntity>(resultContent);
                }
            }

            return requestResult;
        }

        //public static void ConfigureServices(IServiceCollection services)
        //{
        //    services.AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));
        //}
    }
}
