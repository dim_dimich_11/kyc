﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckUserStatus.Enums
{
    public enum UserStatus
    {
        Empty,
        New,
        InProgress,
        Incomplete,
        Low,
        Medium,
        High,
        Fail
    }
}
