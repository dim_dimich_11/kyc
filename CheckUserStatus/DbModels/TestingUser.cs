﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using CheckUserStatus.Enums;

namespace CheckUserStatus.DbModels
{
    public class TestingUser
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string KycOwner { get; set; }

        public string UuId { get; set; }

        public UserStatus Status { get; set; } = UserStatus.New;
    }
}
