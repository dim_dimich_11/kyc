﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace CheckUserStatus.Extensions
{
    public static class HttpClientExtension
    {
        public static void AddHeaders(this HttpClient client, List<KeyValuePair<string, string>> headers)
        {
            if (headers == null)
            {
                return;
            }

            foreach (var header in headers)
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
            }
        }
    }
}
