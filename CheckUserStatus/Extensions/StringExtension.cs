﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckUserStatus.Extensions
{
    public static class StringExtension
    {
        public static string AddParamsToUrl(this string url, List<KeyValuePair<string, string>> parameters)
        {
            if (parameters != null)
            {
                var values = parameters.Select(x => x.Value).ToList();

                url += string.Join("/", values);
            }

            return url;
        }
    }
}
