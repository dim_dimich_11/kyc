﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CheckUserStatus.Infrastructure.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> predicate);
        Task<bool> UpdateOneAsync(TEntity entity);
        Task<List<TEntity>> FindManyAsync();
        Task InsertOneAsync(TEntity entity);
    }
}
